#!/bin/bash

# Autor: Chris Rätsepso, 2016

# Skriptimiskeeled arvestus (bash)

# Antud skript saab parameetritena ette 1.Sisendfail 2.Väljundfail 3.Otsitav String

# Skript loeb sisendfailist reahaaval failide/kaustade nimed.
# Kui sisendfailist loetud reas on kirjas failinimi, siis otsitakse sisendfailist kolmanda parameetrina antud stringi (3. otsitav). 
# Kui string on failis olemas, kirjutatakse väljundfaili rida: sisendfaili_nimi,OLEMAS;vastasel juhul sisendfaili_nimi,POLE OLEMAS

# Kui sisendfailis oleval real oli kataloogi nimi, siis otsitakse selle kataloogi seest faile ja katalooge, 
# millede nimes on kolmanda parameetrina antud string (3. otsitav)
# Kui kausta failide nimes leitakse vastav String, kirjutatakse väljundfaili rida: sisendfaili_nimi,OLEMAS; 
# vastasel juhul sisendfaili_nimi,POLE OLEMAS.

# Kui sisendfailist välja loetud faili/kausta pole olemas, siis kirjutatakse väljundfaili rida: sisendfaili_nimi, POLE OLEMAS 


# Exit koodid:
# 1 - kui sisendfaili ei ole olemas/ei ole fail või ei ole seda võimalik lugemiseks avada
# 2 - kui väljundfaili ei ole võimalik kirjutamiseks avada
# 3 - kui kasutaja ei sisestanud täpselt kolm parametrit



# Kontrollitakse, kas kasutaja sisestas täpselt kolm parameetrit.

if [ $# -ne 3 ]; then
    echo "Kasutamine: $0 SISENDFAIL VÄLJUNDFAIL OTSITAV"
    exit 3
fi

# Parameetrite salvestamine muutujatesse.

SISENDFAIL=$1
VALJUNDFAIL=$2
OTSITAV=$3

# Kontrollitakse, kas sisendfail on fail/on olemas (kui ei ole, siis teavitatakse sellest kasutajale ja skript lõpetab töö)

if [ ! -f $SISENDFAIL ]; then
    echo "Sisendfail $SISENDFAIL ei ole fail või seda ei eksisteeri!"
    exit 1
fi

# Kontrollitakse, kas sisendfaili on võimalik lugemiseks avada (kui ei ole, siis teavitatakse kasutajat ja skript lõpetab töö)

test -r $SISENDFAIL

if [ $? -ne 0 ]; then
   echo "Sisendfaili $SISENDFAIL ei saa lugeda!"
   exit 1
fi

# Kontrollitakse, kas väljundfail on olemas. Kui väljundfail on juba olemas, luuakse uus fail mustriga "failinimi_yyyy-mm-dd_HH-MM[.ext]).
# Kui väljundfaili ei ole olemas, siis luuakse see vastavalt kasutaja poolt antud väljundfaili nimega.

if [  -f $VALJUNDFAIL ]; then
  KUUPAEV=`date +%Y-%m-%d`
  KELLAAEG=`date +%H-%M`
  VALJUND="$VALJUNDFAIL"_"$KUUPAEV"_"$KELLAAEG"
  echo "Uue väljundfaili loomine nimega $VALJUND"
  touch $VALJUND
else
    VALJUND=$VALJUNDFAIL
    touch $VALJUND
fi

# # Kontrollitakse, kas väljundfaili on võimalik kirjutada (kui ei ole, siis teavitatakse kasutajat ja skript lõpetab töö)

test -w $VALJUND

if [ $? -ne 0 ]; then
   echo "Väljundfaili $VALJUND ei saa kirjutada!"
   exit 2
fi

# Väljundfaili esimesele reale kirjutatakse 3. parameetrina antud string.

echo $OTSITAV > $VALJUND

# Sisendfail loetakse reahaaval sisse, kusjuures tühje ridu ignoreeritakse.

for rida in `sed '/^$/d' $SISENDFAIL`; do

# Kui sisendfaili reast loetud fail ei ole fail ega ei ole kaust (e. teda pole olemas),
# siis kirjutatakse väljundfaili rida kujul: sisendfaili_nimi,POLE OLEMAS

    if [ ! -f $rida ] && [ ! -d $rida ]; then
       echo "$rida,POLE OLEMAS" >> $VALJUND
    fi
    
# Kui sisendfaili reast loetud fail on fail, siis otsitakse kasutaja poolt ette antud otsitavat Stringi vastavast failist.
# Kui see leitakse, kirjutatakse väljundfaili rida kujul: sisendfaili_nimi,OLEMAS
# Kui seda ei leita, kirjutatakse väljundfaili rida kujul: sisendfaili_nimi,POLE OLEMAS  
 
    if [ -f $rida ]; then
       grep -wq $OTSITAV $rida 
          if [ $? -eq 0 ]; then
            echo "$rida,OLEMAS" >> $VALJUND
          else
            echo "$rida,POLE OLEMAS" >> $VALJUND
          fi
    fi

# Kui sisendfaili reast loetud fail on kataloog, siis otsitakse selle kataloogi seest faile ja katalooge, 
# millede nimes on kolmanda parameetrina antud string.
# Kui sellise nimega fail/faile leitakse, kirjutatakse väljundfaili rida kujul: sisendfaili_nimi,OLEMAS.
# Kui sellise nimega fail/faile ei leita, kirjutatakse väljundfaili rida kujul: sisendfaili_nimi,POLE OLEMAS .

     if [ -d $rida ]; then
        ls -A1 $rida | grep -w $OTSITAV > /dev/null 2>&1 
        if [ $? -eq 0 ]; then
            echo "$rida,OLEMAS" >> $VALJUND
        else
            echo "$rida,POLE OLEMAS" >> $VALJUND
        fi 
     fi    
done




